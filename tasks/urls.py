from django.urls import path
from tasks.views import create_task_view, show_my_tasks

urlpatterns = [
    path("create/", create_task_view, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
