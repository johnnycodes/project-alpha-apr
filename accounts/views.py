from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import AccountLoginForm, AccountSignupForm


# Create your views here.
def account_login_view(request):
    form = AccountLoginForm
    if request.method == "POST":
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def account_logout_view(request):
    logout(request)
    return redirect("login")


def account_signup_view(request):
    form = AccountSignupForm
    if request.method == "POST":
        form = AccountSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
    context = {"form": form}
    return render(request, "registration/signup.html", context)
